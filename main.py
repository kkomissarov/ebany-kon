import pymorphy2
import random

morph = pymorphy2.MorphAnalyzer()
word = input().lower()
parsing = morph.parse(word)

firsts = [
    'кисло',
    'глисто',
    'тухло',
    'пиздо',
    'срано'
]

seconds = [
    ['дырый', 'дырая', 'дырое'],
    ['пердежный', 'пердежная', 'пердежное'],
    ['залупный', 'залупная', 'залупное'],
    ['блядский', 'блядская', 'блядское']
]

ends = [
    'ебаный конь',
    'блядь',
    'нахуй',
    'жопа сраная',
    'сука'
]


gender_keys = {
    'masc': 0,
    'femn': 1,
    'neut': 2
}

#Является ли слово существительным
is_noun = False

#Проверяем все значения слова, пока не найдем существительное
#Приводим существительное к нормальной словоформе (ед.ч. им.пад)
for result in parsing:
    if 'NOUN' in result.tag:
        normalized = result.normal_form
        gender = result.tag.gender
        is_noun = True
        break



if is_noun:
    first = firsts[random.randint(0, len(firsts)-1)]
    second = seconds[random.randint(0, len(seconds)-1)][gender_keys.get(gender)]
    end = ends[random.randint(0, len(ends)-1)]
    print('{} {}, {}!'.format(first+second, normalized, end).title())
else:
    print('Ваше слово не похоже на существительное. Введите любое существительное.')


